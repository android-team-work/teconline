package com.webtunix.techapp.prelogin;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.webtunix.techapp.R;
import com.webtunix.techapp.adminmodule.activities.WelcomeAdmin;
import com.webtunix.techapp.usermodule.studentactivities.WelcomeStudent;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;

import javax.net.ssl.HttpsURLConnection;

public class SignIn extends AppCompatActivity {

    StringBuilder total, subTotal,devicesubTotal;

    ArrayList<String>arrayList;

    String responseString ="false";

    String userOriginalUserName=null,expDate = null;
Date date;
String fcmNotificationValue;

    TextInputEditText useremail,userpassword;

    String useremailString, userPasswordString,usersavedString,usersavedpassword,savedexpiryData;
    int responseCode;
    ArrayList <String> userType;
    ArrayList <String> userDeatil;

    TextView forgotPassword;
    String deviceCheck,savedDeviceID;

    AwesomeValidation awesomeValidation=null;
    String str,userdetailData,userTypeData,deviceId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);

      /*  Intent intent = getIntent();

        fcmNotificationValue =  intent.getStringExtra("NewNotification");*/

        userDeatil = new ArrayList<String>();

        SharedPreferences sh
 = getSharedPreferences("techapppref",MODE_PRIVATE);

        usersavedString = sh.getString("useremail", "");
        usersavedpassword = sh.getString("userpassword", "");
        savedexpiryData = sh.getString("exp_date", "");
        userTypeData = sh.getString("userType", "");
        userdetailData = sh.getString("userName", "");


        savedDeviceID = sh.getString("FCM", "");
        Log.d("I am outside now ",savedDeviceID+"");
        Log.d(" outside userdetail ",userdetailData+"");

        String re = null;
if(!(usersavedString.equals("nul")&&usersavedpassword.equals("nul"))){
try {


   str = usersavedString.trim() + ":" + usersavedpassword.trim();

    useremailString = usersavedString;
    userPasswordString =usersavedpassword;
   // str = useremailString.trim() + ":" + userPasswordString.trim();

     re = new DeviceList().execute(str).get();

}catch (Exception e){
    
}
    Log.d("I am before  inside ", "saved :"+savedDeviceID+" getting: "+re);


   if(re.equals(savedDeviceID)) {


    Log.d("I am inside now ", "save user string not ull");

    Date date2 = new Date();
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    UserData userData = new UserData();


    try {
        String userDataString = userData.execute(str).get();

        Log.d("I am  200 wali if ", "" + (userDeatil.get(1)));
        Log.d("I am  Date wali if ", "" + df.format(date2));

        //  Toast.makeText(this,"Value : "+userDataString,Toast.LENGTH_LONG).show();
        if (userDataString.equals("200")) {

            String stringDate =df.format(date2).toString();
            Log.d("I am  200 wali if ", "" +(userDeatil.get(1)));
          String[] Currentparts = (stringDate).split("-");
            String year = Currentparts[0];
            String month = Currentparts[1];
            String day  = Currentparts[2];


          String savewaliDate = userDeatil.get(1).toString();

            Log.d("I am  200 wali if ", "" + (userDeatil.get(1)));


              String[] Savedparts = (savewaliDate).split("-");
              String saveyear = Savedparts[0];
              String savemonth = Savedparts[1];
              String saveday = Savedparts[2];



            if ((Integer.parseInt(month)==(Integer.parseInt(savemonth)))  && (Integer.parseInt(day)==(Integer.parseInt(saveday))) && (Integer.parseInt(year)==(Integer.parseInt(saveyear)))&& (userTypeData.trim().equals("0"))) {
                Log.d("I am  Date wali if ", "c "+month+" save "+savemonth);
                Log.d("I am  Date wali if ", "d "+day+" save "+saveday);
                Log.d("I am  Date wali if ", "Y "+year+" save "+saveyear);

                Intent intent3 = new Intent(this, PaymentActivity.class);
                startActivity(intent3);


            }else if ((Integer.parseInt(month)==(Integer.parseInt(savemonth)))  && (Integer.parseInt(day)<(Integer.parseInt(saveday))) && (Integer.parseInt(year)==(Integer.parseInt(saveyear)))&& (userTypeData.trim().equals("0"))) {

                Intent intent1 = new Intent(this, WelcomeStudent.class);
                intent1.putExtra("NewNotification", fcmNotificationValue);
                startActivity(intent1);
            }else if ((Integer.parseInt(month)==(Integer.parseInt(savemonth)))  && (Integer.parseInt(day)>(Integer.parseInt(saveday))) && (Integer.parseInt(year)==(Integer.parseInt(saveyear)))&& (userTypeData.trim().equals("0"))) {
                Intent intent3 = new Intent(this, PaymentActivity.class);
                startActivity(intent3);
                finish();

            }
            else if(Integer.parseInt(month)>=(Integer.parseInt(savemonth))   && Integer.parseInt(year)==Integer.parseInt(saveyear)&& (userTypeData.trim().equals("0"))) {


                if(Integer.parseInt(day)<(Integer.parseInt(saveday))){
                    Intent intent1 = new Intent(this, WelcomeStudent.class);
                    intent1.putExtra("NewNotification", fcmNotificationValue);
                    startActivity(intent1);
                }
             else{
                    Intent intent3 = new Intent(this, PaymentActivity.class);
                    startActivity(intent3);

                }

                /*
                    if(Integer.parseInt(day)<(Integer.parseInt(saveday))){

                            Log.d("Date badi  hai ", "expiry dur");

                            Intent intent1 = new Intent(this, WelcomeStudent.class);
                            intent1.putExtra("NewNotification", fcmNotificationValue);
                            startActivity(intent1);
                        } else if(Integer.parseInt(day)>(Integer.parseInt(saveday))){
                            Log.d("Date chotti  hai "+Integer.parseInt(day), "True date matches exists"+Integer.parseInt(saveday));

                            Intent intent31 = new Intent(SignIn.this, PaymentActivity.class);
                            startActivity(intent31);
                            finish();

                        }else{
                            Intent intent32 = new Intent(SignIn.this, PaymentActivity.class);
                            startActivity(intent32);
                            finish();

                        }
                 */


            }

            else {

                Log.d("All good ", "false date matches exists");

                Intent intent1 = new Intent(this, WelcomeStudent.class);
                intent1.putExtra("NewNotification", fcmNotificationValue);
                startActivity(intent1);
            }


        } else {

            Log.d("I am  200 wali if else ", "user not exists");


            SharedPreferences.Editor myEdit = sh.edit();


            myEdit.putString("useremail", "nul");
            myEdit.putString("userpassword", "nul");

            myEdit.commit();


            Intent intent5 = new Intent(SignIn.this, SignIn.class);
            intent5.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent5);
     //       Toast.makeText(SignIn.this, "You are successfully Logged Out, Thank You", Toast.LENGTH_SHORT).show();

            finish();
        }


    } catch (ExecutionException e) {
        e.printStackTrace();
    } catch (InterruptedException e) {
        e.printStackTrace();
    }


}else {
    SharedPreferences.Editor myEdit = sh.edit();


    myEdit.putString("useremail", "nul");
    myEdit.putString("userpassword", "nul");
       //myEdit.putString("FCM", "nul");


    myEdit.commit();


    Intent intent5 = new Intent(SignIn.this, SignIn.class);
    intent5.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    startActivity(intent5);
   // Toast.makeText(SignIn.this, "You are successfully Logged Out, Thank You", Toast.LENGTH_SHORT).show();

    finish();
}


   /* Intent intent1 = new Intent(this, WelcomeStudent.class);
    intent1.putExtra("NewNotification", fcmNotificationValue);
    startActivity(intent1);*/
}



        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        userType = new ArrayList<String>();

        useremail = findViewById(R.id.signinemail);

        userpassword = findViewById(R.id.signinpassword);
        forgotPassword = findViewById(R.id.forgotPassword);


        useremail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(!(useremail.getText()==null)){

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(useremail.getApplicationWindowToken(), 0);

                }
            }
        });

        userpassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(!(userpassword.getText()==null)){

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(userpassword.getApplicationWindowToken(), 0);

                }
            }
        });

        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String url = getString(R.string.pureServer_link)+"/password_reset/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        awesomeValidation.addValidation(this, R.id.signinemail, android.util.Patterns.EMAIL_ADDRESS, R.string.err_email);

        awesomeValidation.addValidation(this, R.id.signinpassword, ".{8,}", R.string.invalid_password);
    }



    public void openSignUp(View view) {


     /*  Intent intent = new Intent(this, PaymentActivity.class);
        startActivity(intent);*/

  Intent intent = new Intent(this,SignUp.class);
        startActivity(intent);


    }

    public void signIn(View view) {


        if(awesomeValidation.validate()){
            useremailString = useremail.getText().toString();
            userPasswordString = userpassword.getText().toString();



            str = useremailString+":"+userPasswordString;

            DoSomething doSomething = new DoSomething();
            UserData userData = new UserData();

            try {
                responseString = doSomething.execute().get();

            } catch (Exception e) {
                e.printStackTrace();
            }

            Log.i("Vineet Kumar G hun", responseString+ "");
            if(responseString.trim().equals("200")) {
                try {
                   String mainKikrna =  userData.execute(str).get();
                    Log.i("Main ni krna", mainKikrna+ "");

                }catch (Exception e){

                }
                ArrayList<String> userInfo;




           SharedPreferences sharedPreferences = getSharedPreferences("techapppref",MODE_PRIVATE);

                SharedPreferences.Editor myEdit = sharedPreferences.edit();


                myEdit.putString("useremail",useremailString);
                myEdit.putString("userpassword",userPasswordString);

                myEdit.putString("userType",userType.get(0).trim());
               myEdit.putString("superUser",userType.get(1).trim());


                myEdit.putString("userName",userDeatil.get(0).trim());
                 myEdit.putString("exp_date",userDeatil.get(1).trim());
                myEdit.putString("user_id",userDeatil.get(2).trim());
                myEdit.putString("course_category",userDeatil.get(3).trim());



                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

                date = new Date();





                String stringDate =df.format(date).toString();
                Log.d("I am  200 wali if ", "" + (userDeatil.get(1)));
                String[] Currentparts = (stringDate).split("-");
                String year = Currentparts[0];
                String month = Currentparts[1];
                String day  = Currentparts[2];


                String savewaliDate = userDeatil.get(1).toString();

                Log.d("I am  userdeatil ", "" + (userDeatil.get(1)));


                String[] Savedparts = (savewaliDate).split("-");
                String saveyear = Savedparts[0];
                String savemonth = Savedparts[1];
                String saveday = Savedparts[2];





                if(userType.get(0).trim().equals("0")){
                    Log.d("Date braber hai "+df.format(date).toString(), "True date matches exists"+userDeatil.get(1));
                    myEdit.commit();

                    if ((Integer.parseInt(month)==(Integer.parseInt(savemonth)))  && (Integer.parseInt(day)==(Integer.parseInt(saveday))) && (Integer.parseInt(year)==(Integer.parseInt(saveyear)))&& (userType.get(0).trim().equals("0"))) {
                        Log.d("Date braber hai "+df.format(date).toString(), "True date matches exists"+userDeatil.get(1));

                        Intent intent3 = new Intent(this, PaymentActivity.class);
                        startActivity(intent3);
                        finish();


                    }
                    else if ((Integer.parseInt(month)==(Integer.parseInt(savemonth)))  && (Integer.parseInt(day)>(Integer.parseInt(saveday))) && (Integer.parseInt(year)==(Integer.parseInt(saveyear)))&& (userType.get(0).trim().equals("0"))) {
                        Intent intent3 = new Intent(this, PaymentActivity.class);
                        startActivity(intent3);
                        finish();
                    } else if ((Integer.parseInt(month)==(Integer.parseInt(savemonth)))  && (Integer.parseInt(day)<(Integer.parseInt(saveday))) && (Integer.parseInt(year)==(Integer.parseInt(saveyear)))&& (userType.get(0).trim().equals("0"))) {

                        Intent intent1 = new Intent(this, WelcomeStudent.class);
                        intent1.putExtra("NewNotification", fcmNotificationValue);
                        startActivity(intent1);

                    }else if(Integer.parseInt(month)>=(Integer.parseInt(savemonth))   && Integer.parseInt(year)==Integer.parseInt(saveyear)&& (userType.get(0).trim().equals("0"))) {


                        if(Integer.parseInt(day)<(Integer.parseInt(saveday))){

                            Log.d("Date badi  hai ", "expiry dur");

                            Intent intent1 = new Intent(this, WelcomeStudent.class);
                            intent1.putExtra("NewNotification", fcmNotificationValue);
                            startActivity(intent1);
                        }else{
                            Intent intent32 = new Intent(SignIn.this, PaymentActivity.class);
                            startActivity(intent32);
                            finish();

                        }

                    } else if(Integer.parseInt(year)>Integer.parseInt(saveyear)&& (userType.get(0).trim().equals("0"))) {
                        Log.d("I am  Date wali if ", "True date matches exists");

                        Intent intent3 = new Intent(this, PaymentActivity.class);
                        startActivity(intent3);
                        finish();


                    }/*else if(Integer.parseInt(year)<Integer.parseInt(saveyear)&& (userType.get(0).trim().equals("0"))) {
                        Log.d("I am  Date wali if ", "True date matches exists");

                        Intent intent3 = new Intent(this, PaymentActivity.class);
                        startActivity(intent3);
                        finish();

                    }*/


                    else {

                        Toast.makeText(getBaseContext(),"Suceesfully logged as Student",Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(this, WelcomeStudent.class);
                        intent.putExtra("NewNotification", fcmNotificationValue);

                        //     Log.d("Dekho user name AYA ",userDeatil.get(0).trim());

                        myEdit.commit();

                        startActivity(intent);
                    }


             /* Toast.makeText(getBaseContext(),"Suceesfully logged as Student",Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(this, WelcomeStudent.class);
                    intent.putExtra("NewNotification", fcmNotificationValue);

                    //     Log.d("Dekho user name AYA ",userDeatil.get(0).trim());

                    myEdit.commit();

                    startActivity(intent);*/



                }else if(userType.get(0).trim().equals("1")){



                    Intent intent = new Intent(this, WelcomeStudent.class);
                    intent.putExtra("NewNotification", fcmNotificationValue);

                    myEdit.putString("userType",userType.get(0).trim());
                    myEdit.putString("userName",userOriginalUserName);
                    myEdit.putString("exp_date",expDate);

                    myEdit.commit();
                   startActivity(intent);


                }else{

                    Intent intent = new Intent(this, SignIn.class);
                    startActivity(intent);
                }



         /*   Intent intent = new Intent(this, WelcomeStudent.class);
                startActivity(intent);
*/


            }else {

              AlertDialog alertDialog = new AlertDialog.Builder(SignIn.this).create();
                alertDialog.setTitle("Sign In Alert");
                alertDialog.setMessage("Login failed: Invalid Email Address or Password.");
                alertDialog.setIcon(R.mipmap.logo);

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {


                    }
                });

                alertDialog.show();

            }

        }

    }


    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    class DoSomething extends AsyncTask<Void,Void,String> {


        @Override
        protected String doInBackground(Void... voids) {

            try {
                URL obj = new URL(getString(R.string.server_link)+"LoginView");


               JSONObject postDataParams = new JSONObject();
                postDataParams.put("username","");
                postDataParams.put("email", useremailString);
                postDataParams.put("password", userPasswordString);

                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setReadTimeout(15000 /* milliseconds */);
                con.setConnectTimeout(15000 /* milliseconds */);
                con.setRequestMethod("POST");
                con.setDoInput(true);
                con.setDoOutput(true);

                OutputStream os = con.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "utf-8"));

                writer.write(getPostDataString(postDataParams));
                Log.i("paramsData VINIIIII", getPostDataString(postDataParams).toString());


                writer.flush();
                writer.close();
                os.close();

                responseCode = con.getResponseCode();
                if(responseCode== HttpsURLConnection.HTTP_OK) {


                   InputStream inputStream = con.getInputStream();

                    BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
                    total = new StringBuilder();
                    for (String line; (line = r.readLine()) != null; ) {
                        total.append(line).append('\n');
                    }

                    Log.i("Vineet Kumar ", total + "");


                    System.out.println("Response Code :: " + responseCode);

                    Log.i(" Sir Java ", responseCode + "");

           }
           JSONObject jsonObject = new JSONObject(total.toString());
              String expData= jsonObject.optString("user");
             /*   if(expData.trim().equals("user accont expire")){
                    Intent intent = new Intent(SignIn.this, SignIn.class);
                    startActivity(intent);
                    finish();

                }*/

                userType.add(""+jsonObject.optInt("user_type"));
                userType.add(""+jsonObject.optString("is_superuser"));


            } catch (Exception e) {
e.printStackTrace();
return responseCode+"";
            }
            return responseCode+"";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
try {
    JSONObject jsonObject = new JSONObject(total.toString());
    String expData= jsonObject.optString("user");

  /*if(expData.trim().equals("user accont expire")){
        Intent intent = new Intent(SignIn.this, SignIn.class);
        startActivity(intent);
        finish();
    }*/
    userType.add(""+jsonObject.optInt("user_type"));

   userType.add(""+jsonObject.optString("is_superuser"));



        Log.d("USER DATA TYPE !!!! ",""+expData);


}catch(Exception e){


    e.printStackTrace();
    }






        }
    }



    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        Log.i("Vineet Kumar G Ki aaya", result.toString() + "");

        return result.toString();
    }



    class UserData extends AsyncTask<String,Void,String>{


        @Override
        protected String doInBackground(String... strings) {

            int  resportCode=0;


            try {

                byte[] data;
                String base64=null;



                try {

                    data = strings[0].getBytes("UTF-8");

                    base64 = Base64.encodeToString(data, Base64.DEFAULT);

                    Log.i("Base  ", base64);

                } catch (Exception e) {

                    e.printStackTrace();

                }
                URL obj = new URL(getString(R.string.server_link)+"User_Serializers_list?format=json");

                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                con.setReadTimeout(20000);
                con.setConnectTimeout(20000);

                con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                con.setRequestProperty("Expect", "100-continue");
                con.addRequestProperty("Authorization", "Basic "+base64);
               resportCode = con.getResponseCode();
                System.out.println("Response Code :: " + responseCode);


                InputStream inputStream = con.getInputStream();

                BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
                subTotal = new StringBuilder();
                for (String line; (line = r.readLine()) != null; ) {
                    subTotal.append(line).append('\n');
                }

                Log.d("GETTING SUB USER DATA",""+subTotal);
                JSONObject jsonObject = new JSONObject(subTotal.toString());

              JSONObject jsonObject1 = jsonObject.getJSONObject("data");
              userDeatil.add(jsonObject1.optString("username"));
                userDeatil.add(jsonObject1.optString("exp_date"));
                userDeatil.add(jsonObject1.optString("user_id"));
                userDeatil.add(jsonObject1.optString("main_course_category_id"));


            } catch (Exception e) {
                e.printStackTrace();
            }


            return resportCode+"";

        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            try {
                //JSONObject jsonObject = new JSONObject(subTotal.toString());



              /*  aVoid.add(jsonObject.optString("username"));
                aVoid.add(jsonObject.optString("exp_date"));*/






            }catch (Exception e){

            }






        }
    }
    public void clearApplicationData() {
        File cache = getCacheDir();
        File appDir = new File(cache.getParent());
        if (appDir.exists()) {
            String[] children = appDir.list();
            for (String s : children) {
                if (!s.equals("lib")) {
                    deleteDir(new File(appDir, s));
                    Log.i("EEEEEERRRR", "**************** File /data/data/APP_PACKAGE/" + s + " DELETED *******************");
                }
            }
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        return dir.delete();
    }

    class DeviceList extends AsyncTask<String,Void,String>{


        @Override
        protected String doInBackground(String... strings) {

            int  resportCode=0;


            try {

                byte[] data;
                String base64=null;



                try {

                    data = strings[0].getBytes("UTF-8");

                    base64 = Base64.encodeToString(data, Base64.DEFAULT);

                    Log.i("Base  ", base64);

                } catch (Exception e) {

                    e.printStackTrace();

                }
                URL obj = new URL(getString(R.string.server_link)+"User_Device_list");

                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                con.setReadTimeout(20000);
                con.setConnectTimeout(20000);

                con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                con.setRequestProperty("Expect", "100-continue");
                con.addRequestProperty("Authorization", "Basic "+base64);
                resportCode = con.getResponseCode();
                System.out.println("Response Code :: " + responseCode);


                InputStream inputStream = con.getInputStream();

                BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
                 devicesubTotal = new StringBuilder();
                for (String line; (line = r.readLine()) != null; ) {
                    devicesubTotal.append(line).append('\n');
                }

                Log.d("GETTING SUB USER DATA",""+devicesubTotal);
                JSONObject jsonObject = new JSONObject(devicesubTotal.toString());

                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                deviceId = (jsonObject1.optString("registration_id"));

                Log.d("GETTING Registration ID",""+deviceId);



            } catch (Exception e) {
                e.printStackTrace();
            }


            return deviceId+"";

        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            try {
                //JSONObject jsonObject = new JSONObject(subTotal.toString());



              /*  aVoid.add(jsonObject.optString("username"));
                aVoid.add(jsonObject.optString("exp_date"));*/






            }catch (Exception e){

            }






        }
    }

}
