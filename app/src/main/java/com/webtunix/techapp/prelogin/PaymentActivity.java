package com.webtunix.techapp.prelogin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.razorpay.Checkout;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultListener;
import com.razorpay.PaymentResultWithDataListener;
import com.webtunix.techapp.R;
import com.webtunix.techapp.usermodule.studentactivities.ui.modulesubcategory.ModuleSubCategoryAdapter;
import com.webtunix.techapp.usermodule.studentactivities.ui.modulesubcategory.ModuleSubcategoryFragment;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.zip.CheckedOutputStream;

public class PaymentActivity extends AppCompatActivity implements  PaymentResultWithDataListener {

    PaymentAdapter  paymentAdapter;
    RecyclerView  recyclerView;

    private static final String TAG = PaymentActivity.class.getSimpleName();
    TextView textView;
 static Button pay;
    StringBuilder total;

    ArrayList<String> planName;
    ArrayList<String> price;
    ArrayList<String> days;
    ArrayList<Integer> spinnerId;

    static int positions=5;

    String user_id;
    String saved_exp_date;
    String userType;
    String userEmail,userPassword;
RadioGroup radioGroup;
    String new_exp_date;
    String plann, selectedDays;
    String passKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);

        setContentView(R.layout.activity_payment);

        UserData userData = new UserData();


        SharedPreferences sh = getSharedPreferences("techapppref",MODE_PRIVATE);

        user_id = sh.getString("user_id", "");
       // saved_exp_date =sh.getString("exp_date", "");
        userType =sh.getString("userType", "");
        userEmail = sh.getString("useremail", "");
        userPassword = sh.getString("userpassword", "");
       passKey = userEmail.trim()+":"+userPassword.trim();

        try {
         //   String userResponse = userData.execute(passKey).get();

        }catch (Exception e){

        }
        recyclerView = findViewById(R.id.paymentPlanRecycler);
        planName = new ArrayList<String>();
        price = new ArrayList<String>();
        days = new ArrayList<String>();
        spinnerId = new ArrayList<Integer>();
        pay = findViewById(R.id.paynowActivityButton);
        radioGroup = findViewById(R.id.paymentRadioGroup);
        new DoSomething().execute(passKey);

        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

/*
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                Date date = new Date();

                //  new_exp_date = CreateExpDate(saved_exp_date);
                String dateInString = df.format(date);  // Start date
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Calendar c = Calendar.getInstance();
                try {
                    c.setTime(sdf.parse(dateInString));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                c.add(Calendar.DATE, 100);
                sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date resultdate = new Date(c.getTimeInMillis());
                dateInString = sdf.format(resultdate);
                System.out.println("String date:"+dateInString);
                Log.d("after adding :",""+dateInString);
                new_exp_date = dateInString;
                Log.d("New Date : ",""+new_exp_date);*/
startPayment();

            }
        });

    }

    public static  void getValue(int position){

      positions = position;

        pay.setEnabled(true);


    }

    public void startPayment() {


     int cost = Integer.parseInt(price.get(positions));

    plann = planName.get(positions);

    selectedDays = days.get(positions);

            Log.d("days fom Adap:",""+selectedDays);

        Checkout checkout = new Checkout();

        checkout.setImage(R.mipmap.logo);
        final Activity activity = this;

        try {
            JSONObject options = new JSONObject();

            options.put("name", "Tajinder's English Classes");
            options.put("description", ""+plann);
            options.put("currency", "INR");
            /**
             * Amount is always passed in PAISE
             * Eg: "500" = Rs 5.00
             */
            options.put("amount", (cost*100)+"");

            checkout.open(this,options);

        } catch (Exception e) {

        }
    }



    @Override
    public void onPaymentSuccess(String s, PaymentData paymentData) {
        String res = null;

        userType = ""+3;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();

        //  new_exp_date = CreateExpDate(saved_exp_date);
        String dateInString = df.format(date);  // Start date
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(dateInString));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.add(Calendar.DATE, Integer.parseInt(selectedDays.trim()));
        sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date resultdate = new Date(c.getTimeInMillis());
        dateInString = sdf.format(resultdate);
        System.out.println("String date:"+dateInString);
        Log.d("after adding :",""+dateInString);
        new_exp_date = dateInString;
        Log.d("New Date : ",""+new_exp_date);
try {
 res = new UploadingUserInfo().execute().get();

}catch (Exception e){

}
                 if(res.trim().equals("200")){

                     AlertDialog alertDialog = new AlertDialog.Builder(PaymentActivity.this).create();
                     alertDialog.setTitle("Payment Successful Alert");
                     alertDialog.setMessage("Payment Successful ! \n Please Login To Enjoy Our Services \n Thank you");
                     alertDialog.setIcon(R.mipmap.logo);

                     alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                         public void onClick(DialogInterface dialog, int which) {

                             Intent intent = new Intent(PaymentActivity.this, SignIn.class);
                             intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                             startActivity(intent);
                         }
                     });

                     alertDialog.show();



                 }else{


                     AlertDialog alertDialog = new AlertDialog.Builder(PaymentActivity.this).create();
                     alertDialog.setTitle("Payment alert");
                     alertDialog.setMessage("Something Wrong, Payment is not working \n Contact Institute Or Admin");
                     alertDialog.setIcon(R.mipmap.logo);

                     alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                         public void onClick(DialogInterface dialog, int which) {


                         }
                     });
                 }



    }

    @Override
    public void onPaymentError(int i, String s, PaymentData paymentData) {


        Log.d("I tag ",":"+i);
        Log.d("String tag ",":"+s);

        Log.d("payment tag ",":"+paymentData);

    }


    class DoSomething extends AsyncTask<String,Void,Void> {


        @Override
        protected Void doInBackground(String... strings) {

            try {

                byte[] data;
                String base64=null;



                try {

                    data = strings[0].getBytes("UTF-8");

                    base64 = Base64.encodeToString(data, Base64.DEFAULT);

                    Log.i("Base  ", base64);

                } catch (Exception e) {

                    e.printStackTrace();

                }

          //      URL obj = new URL(getString(R.string.server_link)+"Plan_Serializers_list"); old url

                URL obj = new URL(getString(R.string.server_link)+"Plan_Serializers_User");


                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                con.setReadTimeout(20000);
                con.setConnectTimeout(20000);
                con.setRequestProperty("Content-Type", "application/json; charset=utf-8");

                con.setRequestProperty("Expect", "100-continue");
                con.addRequestProperty("Authorization", "Basic "+base64);

                int  responseCode = con.getResponseCode();

                System.out.println("Response Code :: " + responseCode);

                Log.i("Hazoor Sir Java ", responseCode + "");

                InputStream inputStream = con.getInputStream();

                BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
                total = new StringBuilder();
                for (String line; (line = r.readLine()) != null; ) {
                    total.append(line).append('\n');
                }

                Log.i("Vineet Kumar ", total + "");




            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {


                JSONObject jsonObject = new JSONObject(total.toString());

                JSONArray jsonArray = jsonObject.getJSONArray("data");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);


                    planName.add(jsonObject2.optString("name").toString());
                    days.add(jsonObject2.optString("days").toString());

                    price.add(jsonObject2.optString("price").toString());


                    spinnerId.add((jsonObject2.optInt("id")));


                    Log.d("HELLO ARRAYLIST AAYI ",planName.toString());
                    Log.d("HELLO ARRAYLIST AAYI ",spinnerId.toString());


                }

            }catch (Exception e){

            }


           paymentAdapter = new PaymentAdapter(PaymentActivity.this,planName,spinnerId,days,price,radioGroup);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(PaymentActivity.this);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setAdapter(paymentAdapter);





        }
    }


    class UploadingUserInfo extends AsyncTask<String,Void,String> {

        int responseCode;

        protected String doInBackground(String ...strings) {


            try {





                JSONObject postDataParams = new JSONObject();

                postDataParams.put("user_id", user_id);

                postDataParams.put("exp_date", new_exp_date);

                postDataParams.put("user_type", userType.trim());

Log.d("Value to Server ","userID"+user_id+"d"+new_exp_date+"ty"+userType);

                URL obj = new URL(getString(R.string.server_link)+"User_Edit_Api");

                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("POST");

                con.setReadTimeout(20000);
                con.setConnectTimeout(20000);

                con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");


                con.setRequestProperty("Expect", "100-continue");


                con.setDoOutput(true);
                //      con.setDoInput(true);
                OutputStream os = con.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "utf-8"));

                writer.write(getPostDataString(postDataParams));
                Log.i("paramsData VINIIIII FCM", getPostDataString(postDataParams).toString());


                writer.flush();
                writer.close();
                os.close();
                responseCode = con.getResponseCode();
                System.out.println("Response Code :: " + responseCode);



            }catch (Exception e){

            }
            return responseCode+"";

        }

        public String getPostDataString(JSONObject params) throws Exception {

            StringBuilder result = new StringBuilder();
            boolean first = true;

            Iterator<String> itr = params.keys();

            while(itr.hasNext()){

                String key= itr.next();
                Object value = params.get(key);

                if (first)
                    first = false;
                else
                    result.append("&");

                result.append(URLEncoder.encode(key, "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(value.toString(), "UTF-8"));

            }
            Log.i("Vineet Kumar G FCM", result.toString() + "");

            return result.toString();
        }
    }


    String CreateExpDate (String  saved_exp_date){

        int covrertedYear;
        int convertedMonth;
        int convertedDay;
        int selectedDaysInteger = 0;
        Date date = new Date();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String string=null;
        if(saved_exp_date.equals(df.format(date).toString())){
string = saved_exp_date;


        String[] parts = string.split("-");
        String year = parts[0];
        String month = parts[1];
        String day  = parts[2];

           covrertedYear = Integer.parseInt(year);
           convertedMonth = Integer.parseInt(month);

           convertedDay = Integer.parseInt(day);



           Log.d("Converted days :",""+convertedDay);
            Log.d("Converted Month :",""+convertedMonth);

}else {
            string = df.format(date).toString();

            String[] parts = string.split("-");
            String year = parts[0];
            String month = parts[1];
            String day = parts[2];

            covrertedYear = Integer.parseInt(year);
            convertedMonth = Integer.parseInt(month);

            convertedDay = Integer.parseInt(day);


        }


        if (((covrertedYear % 4 == 0) && (covrertedYear % 100!= 0)) || (covrertedYear%400 == 0)) {

                            if(convertedMonth==2){
                                int total_days_inMonth = 29;

                                selectedDaysInteger = Integer.parseInt(selectedDays.trim());


                                Log.d("Slected days :: ",""+selectedDaysInteger);





                                    int exta_days = 29 - convertedDay;

                                    if(exta_days>selectedDaysInteger){

                                        convertedDay= convertedDay+selectedDaysInteger;
                                        return covrertedYear + "-" + convertedMonth + "-" + convertedDay;

                                    }else if(exta_days<=selectedDaysInteger) {

                                        convertedDay = selectedDaysInteger - exta_days;

                                        convertedMonth++;

                                        Log.d("29 wala days",""+convertedDay);

                                        return covrertedYear + "-" + convertedMonth + "-" + convertedDay;
                                    }




                            }

        }
        else
            if(convertedMonth==2) {
                int total_days_inMonth = 28;

                selectedDaysInteger = Integer.parseInt(selectedDays.trim());


                int exta_days = total_days_inMonth - convertedDay;

                if (exta_days > selectedDaysInteger) {

                    convertedDay = convertedDay + selectedDaysInteger;
                    return covrertedYear + "-" + convertedMonth + "-" + convertedDay;

                } else if (exta_days <= selectedDaysInteger) {

                    convertedDay = selectedDaysInteger - exta_days;

                    convertedMonth++;

                    Log.d("28 wala days", "" + convertedDay);

                    return covrertedYear + "-" + convertedMonth + "-" + convertedDay;
                }




            }else if((convertedMonth==1 )||( convertedMonth==3 )||( convertedMonth==5) || (convertedMonth==7) ||(convertedMonth==8)
                    || (convertedMonth==10) || (convertedMonth==12) ){

                int total_days_inMonth = 31;

                selectedDaysInteger = Integer.parseInt(selectedDays.trim());

                Log.d("Slected days :: ",""+selectedDaysInteger);

                Log.d("Coverted days days :: ",""+convertedDay);


                    int exta_days = total_days_inMonth - convertedDay;

                    if (exta_days > selectedDaysInteger) {

                        convertedDay = exta_days + selectedDaysInteger;
                        return covrertedYear + "-" + convertedMonth + "-" + convertedDay;

                    } else if (exta_days < selectedDaysInteger) {

                        convertedDay = total_days_inMonth - exta_days;

                        convertedMonth++;
                        if(convertedMonth==12){

                            covrertedYear = covrertedYear+1;

                        }

                        return covrertedYear + "-" + convertedMonth + "-" + convertedDay;
                    }






                }else if((convertedMonth==4) || (convertedMonth==6) || (convertedMonth==9) || (convertedMonth==11))
                {

                    int total_days_inMonth = 30;

                    selectedDaysInteger = Integer.parseInt(selectedDays.trim());





                        int exta_days = total_days_inMonth - convertedDay;

                        if (exta_days > selectedDaysInteger) {

                            convertedDay = exta_days + selectedDaysInteger;
                            return covrertedYear + "-" + convertedMonth + "-" + convertedDay;

                        } else if (exta_days < selectedDaysInteger) {

                            convertedDay = total_days_inMonth - exta_days;

                            convertedMonth++;

                            return covrertedYear + "-" + convertedMonth + "-" + convertedDay;
                        }



                }





        return covrertedYear + "-" + convertedMonth + "-" + convertedDay;    }



    class UserData extends AsyncTask<String,Void,String>{


        @Override
        protected String doInBackground(String... strings) {

            int  resportCode=0;


            try {

                byte[] data;
                String base64=null;



                try {

                    data = strings[0].getBytes("UTF-8");

                    base64 = Base64.encodeToString(data, Base64.DEFAULT);

                    Log.i("Base  ", base64);

                } catch (Exception e) {

                    e.printStackTrace();

                }
                URL obj = new URL(getString(R.string.server_link)+"User_Serializers_list?format=json");

                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                con.setReadTimeout(20000);
                con.setConnectTimeout(20000);

                con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                con.setRequestProperty("Expect", "100-continue");
                con.addRequestProperty("Authorization", "Basic "+base64);
                resportCode = con.getResponseCode();
                System.out.println("Response Code :: " + resportCode);


                InputStream inputStream = con.getInputStream();

                BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder subTotal = new StringBuilder();
                for (String line; (line = r.readLine()) != null; ) {
                    subTotal.append(line).append('\n');
                }

                Log.d("GETTING SUB USER DATA",""+subTotal);
                JSONObject jsonObject = new JSONObject(subTotal.toString());

                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                saved_exp_date=(jsonObject1.optString("exp_date"));


            } catch (Exception e) {
                e.printStackTrace();
            }


            return resportCode+"";

        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            try {
                //JSONObject jsonObject = new JSONObject(subTotal.toString());



              /*  aVoid.add(jsonObject.optString("username"));
                aVoid.add(jsonObject.optString("exp_date"));*/






            }catch (Exception e){

            }


       }
    }

}
