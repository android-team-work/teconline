package com.webtunix.techapp.adminmodule.adminsearchlist;


import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.webtunix.techapp.R;
import com.webtunix.techapp.usermodule.studentactivities.ui.modulesubcategory.ModuleSubCategoryAdapter;
import com.webtunix.techapp.usermodule.studentactivities.ui.modulesubcategory.ModuleSubcategoryFragment;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdminSearchListFragment extends Fragment {

    AdminSearchListAdapter adminSearchListAdapter;

    ArrayList<Bitmap> bitmaps;
    RecyclerView recyclerView;

    ArrayList<String> arrayList ;
    ArrayList<String> username ;

    ArrayList<Integer> spinnerId ;

    ArrayList<String> phonenumber ;

    StringBuilder total;

AutoCompleteTextView  autoCompleteTextView;

TextView  userTypeAdmin, toptextview;

    View root;
    public AdminSearchListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);

        root = inflater.inflate(R.layout.fragment_admin_search_list, container, false);

String usertype = getArguments().getString("UserType");

        recyclerView  = root.findViewById(R.id.innersubcategorymoduleID);
        arrayList = new ArrayList<String>();
        username = new ArrayList<String>();

        phonenumber = new ArrayList<String>();
        bitmaps = new ArrayList<Bitmap>();

        spinnerId = new ArrayList<Integer>();

        recyclerView = root.findViewById(R.id.adminSearchListRecycleId);
        userTypeAdmin = root.findViewById(R.id.userTypeTextViewadmin);
        autoCompleteTextView = root.findViewById(R.id.searchAutoTextView);


         userTypeAdmin.setText("USER TYPE : "+usertype);

              autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                  @Override
                  public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                          long arg3) {
                    String data = (String) arg0.getAdapter().getItem(arg2);
Log.d("String Data",""+data);


                      int id =   username.indexOf(data);
             spinnerId.get(arg2);
                      Log.d("String arg",""+id);


                      Bundle bundleString = new Bundle();
                      bundleString.putString("selectuser",""+  spinnerId.get(id));
                      Navigation.findNavController(root).navigate(R.id.adminUserDeatilFragment,bundleString);



                  }
              });

        autoCompleteTextView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {


                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(autoCompleteTextView.getApplicationWindowToken(), 0);


            }
        });

      DoSomething doSomething = new DoSomething();

        doSomething.execute();



        return root;
    }
    class DoSomething extends AsyncTask<Void,Void,Void> {


        @Override
        protected Void doInBackground(Void... voids) {

            try {


                String url = getArguments().getString("URL");
                URL obj = new URL(url);

                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                con.setReadTimeout(20000);
                con.setConnectTimeout(20000);
                con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                con.setRequestProperty("Expect", "100-continue");
                int  responseCode = con.getResponseCode();
                System.out.println("Response Code :: " + responseCode);

                Log.i("Hazoor Sir Java ", responseCode + "");

                InputStream inputStream = con.getInputStream();

                BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
                total = new StringBuilder();
                for (String line; (line = r.readLine()) != null; ) {
                    total.append(line).append('\n');
                }

                Log.i("Vineet Kumar ", total + "");




            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {


                JSONObject jsonObject = new JSONObject(total.toString());

                JSONArray jsonArray = jsonObject.getJSONArray("data");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);

                arrayList.add(jsonObject2.optString("email").toString());
                    username.add(jsonObject2.optString("username").toString());

                    phonenumber.add(jsonObject2.optString("mobile").toString());
                    spinnerId.add((jsonObject2.optInt("user_id")));

/*
username
                    Log.d("HELLO ARRAYLIST AAYI ",arrayList.toString());
                    Log.d("HELLO ARRAYLIST AAYI ",imageurl.toString());
*/

                    Log.d("HELLO Spinner AAYI ",spinnerId.toString());
                }

            }catch (Exception e){

            }


     adminSearchListAdapter = new AdminSearchListAdapter(getContext(),arrayList,spinnerId,phonenumber,autoCompleteTextView);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setAdapter(adminSearchListAdapter);


            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,username);
            autoCompleteTextView.setAdapter(adapter);

        }
    }
}
