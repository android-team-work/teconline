package com.webtunix.techapp.usermodule.studentactivities.ui.send;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.webtunix.techapp.R;
import com.webtunix.techapp.usermodule.studentactivities.ui.home.HomeAdapter;
import com.webtunix.techapp.usermodule.studentactivities.ui.home.HomeFragment;
import com.webtunix.techapp.usermodule.studentactivities.ui.home.HomeViewModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class SendFragment extends Fragment {



    private SendViewModel sendViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        sendViewModel =
                ViewModelProviders.of(this).get(SendViewModel.class);
        View root = inflater.inflate(R.layout.fragment_send, container, false);


        TextView textView = root.findViewById(R.id.text_send);



        textView.setText("We are the most recommeneded teaching team! Winners of Best Regional Partner Awards from IDP Australia (India) and British Council\n" +
                "Tajinder's English Classes - Phase 7 Mohali Punjab\n" +
                "\n" +
                "IELTS, PTE and OET have become the global exams to check the English language proficiency of the students. Each exam has a set pattern, scoring system and domain.\n" +
                "\n" +
                "Both IELTS and PTE are now the two parallel bridges to go to English speaking countries for the purpose of studies or permanent residency. The former is 9 Band scale whereas the later one is 90 marks scale.\n" +
                "\n" +
                "Each of the exam has specific question types broadly divided into language modules of Speaking, Writing, Listening and Reading.\n" +
                "\n" +
                "IELTS and PTE are equally valid for Australia and New Zealand for both the study and PR whereas not all Canadian universities accept PTE.\n" +
                "\n" +
                "OET on the other hand is specific exam for medical professionals especially for nurses who want to pursue their career abroad.\n" +
                "\n" +
                "The UK has come up with its own testing format named UKVI on the similar skills and strategies of IELTS.\n" +
                "\n" +
                "We at Tajinder’s English Classes have mastered these techniques by understanding the right criteria over the years by experimenting, testing and training. The teacher training sessions conducted by British Council and IDP Australia have further groomed our skills.\n" +
                "\n" +
                "Our batches are open from 8 AM to 9 PM (Mon-Fri), Sat (8 am to 2 pm) with each student divided by his caliber and capacity.\n" +
                "\n" +
                "We build natural English skills of the students so that able to cope up with any question type they face in the exams.\n" +
                "\n" +
                "We are very proud to say that we are the most recommended teaching team for IELTS, PTE and Spoken English in traditional or online mode.\n" +
                "\n" +
                "I and my team are totally focused on skills and strategies of these exams.\n" +
                "\n" +
                "Meet us personally and start preparing with us.\n" +
                "\n" +
                "Happy English!\n" +
                "\n" +
                "Tajinder Pal Kaur\n" +
                "(M.A. English B.Ed.) ");

            return root;
    }


}
