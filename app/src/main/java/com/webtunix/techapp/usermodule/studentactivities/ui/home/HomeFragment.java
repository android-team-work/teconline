package com.webtunix.techapp.usermodule.studentactivities.ui.home;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.webtunix.techapp.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;


public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;

    HomeAdapter homeAdapter;

    ArrayList<Bitmap> bitmaps;
    RecyclerView recyclerView;

    ArrayList<String> arrayList ;
    ArrayList<String> userDeatil ;

    ArrayList<Integer> spinnerId ;

    ArrayList<String> imageurl ;

    StringBuilder total;
    int  responseCode;
    String isSuperUser;
    String userName,userPassword,authString,responseAfterExecution,courseCategory,s4;

    NavController navController;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        SharedPreferences sh = getActivity().getSharedPreferences("techapppref", Context.MODE_PRIVATE);
        isSuperUser =sh.getString("superUser", "");



View view  = inflater.inflate(R.layout.fragment_home,container,false);


         recyclerView  = view.findViewById(R.id.mainCategoryID);
        arrayList = new ArrayList<String>();
        imageurl = new ArrayList<String>();
        bitmaps = new ArrayList<Bitmap>();
        userDeatil  = new ArrayList<String>();

        homeViewModel = new HomeViewModel();
        spinnerId = new ArrayList<Integer>();



        userName = sh.getString("useremail", "");
        userPassword = sh.getString("userpassword", "");

        courseCategory =sh.getString("course_category", "");

        s4 =sh.getString("userType", "");
Log.d("User TYpe ",""+s4);
        authString = userName.trim()+":"+userPassword.trim();
        String str = null;

UserData userData = new UserData();
try{
     str  = userData.execute(authString).get();

}catch(Exception e){

        }


        DoSomething doSomething = new DoSomething();

        if(s4.trim().equals("1")) {

            String url="Main_Course_Category_list?format=json";
            doSomething.execute(url);

        }else{
            if(str.equals("200")){

                if(userDeatil.get(3).equals(courseCategory)) {
                    String url = "Main_Course_Category_Singal_list/" + courseCategory;
                    doSomething.execute(url);
                }else{
                    String url = "Main_Course_Category_Singal_list/" + userDeatil.get(3);
                    doSomething.execute(url);
                }

            }else{
                String url="Main_Course_Category_Singal_list/"+courseCategory;
                doSomething.execute(url);
            }


        }

 /*   if(!(responseAfterExecution.trim().equals(200))){

        navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);

        navController.navigate(R.id.nav_share);


    }
*/

        Log.d("Chaleya Ki nahi ",arrayList.toString());



        return view;
    }
    public static  void getNameValue(int position){

  }



    class DoSomething extends AsyncTask<String, Void, String> {


        @Override
        protected String doInBackground(String... strings) {

            try {



                URL obj = new URL(getString(R.string.server_link) + strings[0]);

                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                con.setReadTimeout(20000);
                con.setConnectTimeout(20000);
                con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                con.setRequestProperty("Expect", "100-continue");

                responseCode = con.getResponseCode();
                System.out.println("Response Code :: " + responseCode);

                Log.i("Hazoor Sir Java ", responseCode + "");

                InputStream inputStream = con.getInputStream();

                BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
                total = new StringBuilder();
                for (String line; (line = r.readLine()) != null; ) {
                    total.append(line).append('\n');
                }

                Log.i("Vineet Kumar ", total + "");


            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            try {


                JSONObject jsonObject = new JSONObject(total.toString());

                JSONArray jsonArray = jsonObject.getJSONArray("data");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);


                    arrayList.add(jsonObject2.optString("title").toString());
                    imageurl.add(getString(R.string.pureServer_link) + jsonObject2.optString("image").toString());

                    spinnerId.add((jsonObject2.optInt("id")));


                    Log.d("HELLO ARRAYLIST AAYI ", arrayList.toString());
                    Log.d("HELLO ARRAYLIST AAYI ", imageurl.toString());


                }

            } catch (Exception e) {

            }


            homeAdapter = new HomeAdapter(getActivity(), arrayList, imageurl, spinnerId);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setAdapter(homeAdapter);


        }
    }
    class UserData extends AsyncTask<String,Void,String>{


        @Override
        protected String doInBackground(String... strings) {

            int  resportCode=0;


            try {

                byte[] data;
                String base64=null;



                try {

                    data = strings[0].getBytes("UTF-8");

                    base64 = Base64.encodeToString(data, Base64.DEFAULT);

                    Log.i("Base  ", base64);

                } catch (Exception e) {

                    e.printStackTrace();

                }
                URL obj = new URL(getString(R.string.server_link)+"User_Serializers_list?format=json");

                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                con.setReadTimeout(20000);
                con.setConnectTimeout(20000);

                con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                con.setRequestProperty("Expect", "100-continue");
                con.addRequestProperty("Authorization", "Basic "+base64);
                resportCode = con.getResponseCode();
                System.out.println("Response Code :: " + responseCode);


                InputStream inputStream = con.getInputStream();

                BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
               StringBuilder subTotal = new StringBuilder();
                for (String line; (line = r.readLine()) != null; ) {
                    subTotal.append(line).append('\n');
                }

                Log.d("GETTING SUB USER DATA",""+subTotal);
                JSONObject jsonObject = new JSONObject(subTotal.toString());

                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                userDeatil.add(jsonObject1.optString("username"));
                userDeatil.add(jsonObject1.optString("exp_date"));
                userDeatil.add(jsonObject1.optString("user_id"));
                userDeatil.add(jsonObject1.optString("main_course_category_id"));


            } catch (Exception e) {
                e.printStackTrace();
            }


            return resportCode+"";

        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            try {
                //JSONObject jsonObject = new JSONObject(subTotal.toString());



              /*  aVoid.add(jsonObject.optString("username"));
                aVoid.add(jsonObject.optString("exp_date"));*/






            }catch (Exception e){

            }






        }
    }
}