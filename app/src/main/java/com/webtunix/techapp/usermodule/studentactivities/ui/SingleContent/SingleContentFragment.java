package com.webtunix.techapp.usermodule.studentactivities.ui.SingleContent;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.os.PowerManager;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.webtunix.techapp.R;
import com.webtunix.techapp.prelogin.SignIn;
import com.webtunix.techapp.prelogin.SignUp;
import com.webtunix.techapp.prelogin.Splash;
import com.webtunix.techapp.usermodule.studentactivities.WelcomeStudent;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.Inflater;

import static android.media.AudioAttributes.ALLOW_CAPTURE_BY_NONE;

/**
 * A simple {@link Fragment} subclass.
 */
public class SingleContentFragment extends Fragment {

Button pdfHomeSetting;
EditText pdfgoEdittext;
Button toggleButton;
View splitLine;
    Handler mHandler;
    private static MediaRecorder recorderOne;

    RelativeLayout  relativeLayout;

    ArrayList<String> arrayList;
    ArrayList<Integer> spinnerId;

    ArrayList<String> imageurl;
    ArrayList<String> videolist;
    ArrayList<String> descriptionList;

    ArrayList<String> pdflist;
    ProgressDialog mProgressDialog;

    StringBuilder total;

    AudioAttributes audioAttributes;
    Context context;

    PDFView pdfView;
    ImageView imageView;

    TextView detailView, titletextview;
    LinearLayout bottomNavigationView;
RelativeLayout relativeLayoutVideoWala;

    View view=null;
    YouTubePlayerFragment youtubeFragment=null;
    AudioFocusRequest    focusRequest;
    VideoView videoView;

    LinearLayout linearLayout;

    FrameLayout frameLayout;
    TextView mainHeading;
    String pdfMode;

    boolean pdfmodebool=false;
// YouTUbe Player Fargment

    private YouTubePlayer YPlayer;
    private static final String YoutubeDeveloperKey = "AIzaSyAqVTpWcJ9a6-HjsTcNyT_oAmLEYFArJbU";
    private static final int RECOVERY_DIALOG_REQUEST = 1;

    String reg = "(?:youtube(?:-nocookie)?\\.com\\/(?:[^\\/\\n\\s]+\\/\\S+\\/|(?:v|e(?:mbed)?)\\/|\\S*?[?&]v=)|youtu\\.be\\/)([a-zA-Z0-9_-]{11})";

//audio Controls

    LinearLayout audioLinearLayout;

    TextView audioHeading, audioTimeshow;

    SeekBar audioSeekBar;

    Button floatingActionButtonAudio;
    AudioManager audioManager;
    MediaPlayer  mediaPlayer;
    boolean wasPlaying = false;
    View overlay;
    FrameLayout frameLayout1;
    public static final int DIALOG_DOWNLOAD_PROGRESS = 0;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);

       /* audioManager =
                (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        if (audioManager!=null) {
            audioManager.setMicrophoneMute(true);
        }*/

        SharedPreferences sh = getActivity().getSharedPreferences("techapppPDF",Context.MODE_PRIVATE);

                                pdfMode = sh.getString("pdfMode","");

                                if(pdfMode.equals("OFF")){
                                    pdfmodebool=false;


                                }else if(pdfMode.equals("ON")){
                                    pdfmodebool = true;
                                }



        youtubeFragment = null;


        view   = inflater.inflate(R.layout.fragment_single_content, container, false);


        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Loading PDF... \n Please wait ");
        mProgressDialog.setIndeterminate(true);

        mProgressDialog.setCanceledOnTouchOutside(false);

        //audio controls
relativeLayout = view.findViewById(R.id.singleActionBar);
        audioLinearLayout = view.findViewById(R.id.audioLinearLayout);
        audioHeading = view.findViewById(R.id.audioHeading);
        audioTimeshow = view.findViewById(R.id.audioTimeShow);
        audioSeekBar = view.findViewById(R.id.audioseekbar);
        floatingActionButtonAudio = view.findViewById(R.id.auidoPlayPause);
relativeLayoutVideoWala = view.findViewById(R.id.RelativeContentSingle);

bottomNavigationView = (LinearLayout) getActivity().findViewById(R.id.bottomViewRealtiveLayout);
        mediaPlayer = new MediaPlayer();

        pdfHomeSetting = view.findViewById(R.id.pdfHomeSetting);
        toggleButton = view.findViewById(R.id.pdfSwipeButtonEnable);
        pdfgoEdittext = view.findViewById(R.id.pdfgotosetting);
        splitLine = view.findViewById(R.id.SplitLine_hor1);
        videoView = view.findViewById(R.id.videoplayer);


        pdfHomeSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                pdfView.jumpTo(0);
            }
        });

        toggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                   if(pdfgoEdittext.getText().toString().equals("")){
                       AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                       alertDialog.setTitle("Single Content alert");
                       alertDialog.setMessage("Please enter some value");
                       alertDialog.setIcon(R.mipmap.logo);

                       alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                           public void onClick(DialogInterface dialog, int which) {


                           }
                       });

                       alertDialog.show();


                   }else{
                       pdfView.jumpTo( Integer.parseInt(pdfgoEdittext.getText().toString()));

                   }


            }
        });


       overlay = view.findViewById(R.id.singleframelayoutMain);





        arrayList = new ArrayList<String>();
        imageurl = new ArrayList<String>();
        videolist = new ArrayList<String>();
        pdflist = new ArrayList<String>();
        spinnerId = new ArrayList<Integer>();
        descriptionList = new ArrayList<String>();




    linearLayout = view.findViewById(R.id.VideoLinearLayout);
    frameLayout = view.findViewById(R.id.pdfFrame);
        pdfView = view.findViewById(R.id.pdfView);
        /*   imageView = view.findViewById(R.id.innerdescrtionImageview);*/
        //  textView = view.findViewByIdToggleButton(R.id.innerdescriptionTextView);
        titletextview = view.findViewById(R.id.singlecontentTitleTextView);

        mainHeading = view.findViewById(R.id.SingleContentMainTitleTextView);

        detailView = view.findViewById(R.id.singlecontentDetailTextView);
        imageView = view.findViewById(R.id.contentDeatilImageView);
     /*  youtubeFragment = (YouTubePlayerFragment)
         getActivity().getFragmentManager().findFragmentById(R.id.youtubeFragment);*/

       DoSomething doSomething = new DoSomething();

        doSomething.execute();


        Log.d("Chaleya Ki Module wala", arrayList.toString());


floatingActionButtonAudio.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        playSong();
    }
});

        audioSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {

             /*   if(mediaPlayer != null && !mediaPlayer.isPlaying()){
                    mediaPlayer.seekTo(progress * 1000);
                }*/
              //  mediaPlayer.seekTo(seekBar.getProgress());
                audioTimeshow.setVisibility(View.VISIBLE);
                int x = (int) Math.ceil(progress / 1000f);

                if (x < 10)
                    audioTimeshow.setText("0:0" + x);
                else
                    audioTimeshow.setText("0:" + x);

                double percent = progress / (double) audioSeekBar.getMax();
                int offset = seekBar.getThumbOffset();
                int seekWidth = seekBar.getWidth();
                int val = (int) Math.round(percent * (seekWidth - 2 * offset));
                int labelWidth = audioTimeshow.getWidth();
                audioTimeshow.setX(offset + seekBar.getX() + val
                        - Math.round(percent * offset)
                        - Math.round(percent * labelWidth / 2));


                if (progress > 0 && mediaPlayer != null && !mediaPlayer.isPlaying()) {
                    clearMediaPlayer();
                    floatingActionButtonAudio.setBackgroundResource(R.drawable.play);
                   seekBar.setProgress(progress);
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

                audioTimeshow.setVisibility(View.VISIBLE);


            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {



                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    mediaPlayer.seekTo(seekBar.getProgress());
                }
            }



        });



    return view;
    }





    @Override
    public void onPause() {
        super.onPause();
        clearMediaPlayer();



    }
    public static boolean getMicrophoneAvailable(Context context) {
        recorderOne = new MediaRecorder();
        recorderOne.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorderOne.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
        recorderOne.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
        recorderOne.setOutputFile(new File(context.getCacheDir(), "MediaUtil#micAvailTestFile").getAbsolutePath());
        boolean available = true;
        try {
            recorderOne.prepare();
            recorderOne.start();

        }

        catch (Exception exception) {
            available = false;
        }
        return available;
    }
    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    public void onResume() {
        super.onResume();
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);


           /* if( getMicrophoneAvailable(getContext())){

                Log.e("BHAJI USE KITA","NJAHI USE KITA");
            }else{
            //    explain("You need to Off your audio recorders to enjoy TECOnline. Do you want to go to app settings?");

            }*/


     floatingActionButtonAudio.setBackgroundResource(R.drawable.play);
   /*
        AudioManager audioManager =
                (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
     int id =  audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);




        if (audioManager!=null) {

            audioManager.setMicrophoneMute(true);
        }




    }
    @Override
    public void onAudioFocusChange(int focusChange) {

        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_GAIN:

                Log.d("GAIN GAIN","GAIN1");
                mediaPlayer.start();

                break;
            case AudioManager.AUDIOFOCUS_LOSS:
                Log.d("GAIN GAIN","GAIN2");

                mediaPlayer.stop();

                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                Log.d("GAIN GAIN","GAIN3");

                mediaPlayer.stop();

                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                Log.d("GAIN GAIN","GAIN4");

                mediaPlayer.stop();
                break;
        }*/

    }
    private void explain(String msg){
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        //  permissionsclass.requestPermission(type,code);

                        Intent intent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);                  }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        paramDialogInterface.cancel();
                        getActivity().finish();
                    }
                });
        dialog.show();
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        clearMediaPlayer();

   /*     youtubeFragment  = (YouTubePlayerFragment) getActivity().getFragmentManager()
                .findFragmentById(R.id.youtubeFragment);
        if (youtubeFragment != null)
            getActivity().getFragmentManager().beginTransaction().remove(youtubeFragment).commit();*/
    }



    class DoSomething extends AsyncTask<Void, Void, Void> {

        InputStream io;
        @Override
        protected Void doInBackground(Void... voids) {

            try {
                URL obj = new URL(getString(R.string.server_link)+"Course_Content_Serializers_Single/"+getArguments().getString("InnerModuleSubCategory"));


                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                con.setReadTimeout(20000);
                con.setConnectTimeout(20000);
                con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                con.setRequestProperty("Expect", "100-continue");
                int responseCode = con.getResponseCode();


                System.out.println("Response Code :: " + responseCode);

                Log.i("Hazoor Sir Java ", responseCode + "");

                InputStream inputStream = con.getInputStream();

                BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
                total = new StringBuilder();
                for (String line; (line = r.readLine()) != null; ) {
                    total.append(line).append('\n');
                }

                Log.i("Vineet Kumar ", total + "");



            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @SuppressLint("SourceLockedOrientationActivity")
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {


                JSONObject jsonObject = new JSONObject(total.toString());

                JSONArray jsonArray = jsonObject.getJSONArray("data");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);


                    arrayList.add(jsonObject2.optString("title").toString());

                    imageurl.add(getString(R.string.pureServer_link)+jsonObject2.optString("image").toString());
                    videolist.add(getString(R.string.pureServer_link)+jsonObject2.optString("video2").toString());

                    pdflist.add(getString(R.string.pureServer_link)+jsonObject2.optString("pdf").toString());

                    descriptionList.add(jsonObject2.optString("details").toString());

                    spinnerId.add((jsonObject2.optInt("id")));


                    Log.d("HELLO ARRAYLIST AAYI ", imageurl.toString());
                    Log.d("HELLO ARRAYLIST AAYI ", videolist.toString());


                }

            } catch (Exception e) {

            }


            if(!(arrayList.get(0).equals(getString(R.string.pureServer_link)+"null"))){

                mainHeading.setText(arrayList.get(0).toString());

                titletextview.setText(arrayList.get(0).toString());


            } else {
                mainHeading.setText("Couse Contents");

                titletextview.setText("Course Contents");

            }

               if(!videolist.get(0).equals(getString(R.string.pureServer_link)+"null")) {


                   linearLayout.setVisibility(View.VISIBLE);
                   videoView.setVisibility(View.VISIBLE);
                   frameLayout.setVisibility(View.GONE);


                   Log.d("Video  WALA ELSE", "Video  IS NOT  EMPTY");
                   Log.d("Video  WALA ELSE", "Video Link "+videolist.get(0)+"V");
                   Log.d("PDF URL",pdflist.get(0).toString());

                      String url = videolist.get(0);
                    if(url.contains(".mp3"))
                    {
                        audioLinearLayout.setVisibility(View.VISIBLE);

                        videoView.setVisibility(View.GONE);




                        imageView.setVisibility(View.GONE);
                        pdfHomeSetting.setVisibility(View.GONE);
                        toggleButton.setVisibility(View.GONE);
                        pdfgoEdittext.setVisibility(View.GONE);
                        splitLine.setVisibility(View.GONE);
                        titletextview.setVisibility(View.GONE);
                        detailView.setVisibility(View.GONE);
                      /*  titletextview.setText(arrayList.get(0).toString());
                        detailView.setText(descriptionList.get(0).toString());*/

                                   Log.d("MP3","MP3");
                    }
                  else {

                   Log.d("MP4", "MP4");
                        relativeLayoutVideoWala.setVisibility(View.VISIBLE);
                   bottomNavigationView.setVisibility(View.GONE);
                        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,

                                WindowManager.LayoutParams.FLAG_FULLSCREEN);
                        // making it full screen
                        relativeLayout.setVisibility(View.GONE);
                 //       getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
                        Uri uri = Uri.parse(videolist.get(0).toString());

                   MediaController mediaController = new MediaController(getActivity());
                   mediaController.setAnchorView(videoView);
                   videoView.setMediaController(mediaController);
                   videoView.setSecure(true);
                   videoView.setVideoURI(uri);
                   //videoView.requestFocus();

                   videoView.start();
                        final ProgressDialog progDailog = ProgressDialog.show(getActivity(), "Please wait ...", "Retrieving data ...", true);

                        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                            public void onPrepared(MediaPlayer mp) {
                                // TODO Auto-generated method stub
                                progDailog.dismiss();
                            }
                        });

                   imageView.setVisibility(View.GONE);
                   pdfHomeSetting.setVisibility(View.GONE);
                   toggleButton.setVisibility(View.GONE);
                   pdfgoEdittext.setVisibility(View.GONE);
                   splitLine.setVisibility(View.GONE);
                        titletextview.setVisibility(View.GONE);
                        detailView.setVisibility(View.GONE);
                /*   titletextview.setText(arrayList.get(0).toString());
                   detailView.setText(descriptionList.get(0).toString());*/


               }}

            else if(!(pdflist.get(0).equals(getString(R.string.pureServer_link)+"null"))){
                            frameLayout.setVisibility(View.VISIBLE);
                   bottomNavigationView.setVisibility(View.GONE);

                   linearLayout.setVisibility(View.GONE);
                    pdfHomeSetting.setVisibility(View.VISIBLE);
                    toggleButton.setVisibility(View.VISIBLE);
                    pdfgoEdittext.setVisibility(View.VISIBLE);
                    splitLine.setVisibility(View.VISIBLE);

                    Log.d("PDF MODE ",pdfmodebool+"");

                   try {
                       String s =   new DrawPDF().execute().get();


                   } catch (ExecutionException e) {
                       e.printStackTrace();
                   } catch (InterruptedException e) {
                       e.printStackTrace();
                   }


                   Log.d("PDF URL",pdflist.get(0).toString());








               }else{
                   linearLayout.setVisibility(View.VISIBLE);
                   videoView.setVisibility(View.GONE);
                   frameLayout.setVisibility(View.GONE);
                     audioLinearLayout.setVisibility(View.GONE);




                   imageView.setVisibility(View.VISIBLE);
                   Glide.with(getContext())
                           .load(imageurl.get(0))
                           .diskCacheStrategy(DiskCacheStrategy.ALL)
                           .into(imageView);
                   pdfHomeSetting.setVisibility(View.GONE);
                   toggleButton.setVisibility(View.GONE);
                   pdfgoEdittext.setVisibility(View.GONE);
                   splitLine.setVisibility(View.GONE);
                   titletextview.setText(arrayList.get(0).toString());
                   detailView.setText(descriptionList.get(0).toString());
               }
        }

        String getVideoId(String videoUrl) {
            if (videoUrl == null || videoUrl.trim().length() <= 0)
                return null;

            Pattern pattern = Pattern.compile(reg, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(videoUrl);

            if (matcher.find())
                return matcher.group(1);
            return null;
        }
    }

                   class DrawPDF extends AsyncTask<Void, Void, String>{

                       InputStream io;
                       int i;
                       private PowerManager.WakeLock mWakeLock;

                       @Override
                       protected void onPreExecute() {
                           super.onPreExecute();
                           // take CPU lock to prevent CPU from going off if the user
                           // presses the power button during download

                           mProgressDialog.show();
                       }
                       @Override
                       protected String doInBackground(Void... voids) {

                           int count;
                     try {
                              io = new URL(pdflist.get(0).toString()).openStream();

                             Log.d("io get",""+io.available());

                             i = io.available();
                         }catch (Exception e){

                         }
                           return i+"";

                       }

                       private void publishProgress(String ...s) {
                       }

                       @Override
                       protected void onPostExecute(String aVoid) {
                           super.onPostExecute(aVoid);


                           try {




                               pdfView.fromStream(io)

                                       .enableSwipe(true) // allows to block changing pages using swipe
                                       .swipeHorizontal(false)
                                       .onLoad(new OnLoadCompleteListener() {
                                           @Override
                                           public void loadComplete(int nbPages) {
                                               mProgressDialog.dismiss();
                                           }
                                       })
                                       .enableDoubletap(true)
                                       .pageFling(true)
                                       .nightMode(pdfmodebool)
                                       .fitEachPage(true)
                                       .load();



                           }catch (Exception e){

                           }



                       }
                   }

    public void playSong() {

        try {


            if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                clearMediaPlayer();
                audioSeekBar.setProgress(0);
                wasPlaying = true;
                floatingActionButtonAudio.setBackgroundResource(R.drawable.play);
            }


            if (!wasPlaying) {

                if (mediaPlayer == null) {
                    mediaPlayer = new MediaPlayer();
                }
                floatingActionButtonAudio.setBackgroundResource(R.drawable.pouse);
                Uri uri = Uri.parse(videolist.get(0).toString());


                /*Uri mp3 = Uri.parse("android.resource://"

                        + getActivity().getPackageName() + "/raw/"
                        + "sample");*/

                mediaPlayer = new MediaPlayer();

                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

                mediaPlayer.setDataSource(getContext(), uri);

                final ProgressDialog progDailog = ProgressDialog.show(getActivity(), "Please wait ...", "Retrieving data ...", true);

                mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                    public void onPrepared(MediaPlayer mp) {
                        // TODO Auto-generated method stub
                        progDailog.dismiss();
                        mp.start();
                    }
                });
mediaPlayer.prepareAsync();

                mediaPlayer.setVolume(0.5f, 0.5f);
                mediaPlayer.setLooping(false);
                audioSeekBar.setMax(mediaPlayer.getDuration());

             //   mediaPlayer.start();
                mHandler = new Handler();

                new Thread().start();

            }


            wasPlaying = false;
        } catch (Exception e) {
            e.printStackTrace();

        }
    }




    public void run() {

        int currentPosition = mediaPlayer.getCurrentPosition();
        int total = mediaPlayer.getDuration();


        while (mediaPlayer != null && mediaPlayer.isPlaying() && currentPosition < total) {
            try {
                Thread.sleep(1000);
                currentPosition = mediaPlayer.getCurrentPosition();
            } catch (InterruptedException e) {
                return;
            } catch (Exception e) {
                return;
            }

            audioSeekBar.setProgress(currentPosition);

        }
        }




    private void clearMediaPlayer() {
        if(mediaPlayer!=null){
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }else{
            mediaPlayer = null;

        }
        }




    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    public void onDestroy() {
        super.onDestroy();

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        bottomNavigationView.setVisibility(View.VISIBLE);

        relativeLayoutVideoWala.setVisibility(View.GONE);


    }

}