package com.webtunix.techapp.usermodule.studentactivities.ui.innermodulesubcategory;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.webtunix.techapp.R;
import com.webtunix.techapp.usermodule.studentactivities.ui.modulesubcategory.ModuleSubCategoryAdapter;

import java.util.ArrayList;

public class InnerModuleSubcategoryAdapter extends  RecyclerView.Adapter<InnerModuleSubcategoryAdapter.ViewHolder> {

    ArrayList<String> arrayList = new ArrayList<String>();
    ArrayList<String> descriptionList = new ArrayList<String>();

    ArrayList<String> imageurl = new ArrayList<String>();
    ArrayList<Integer> spinnerid = new ArrayList<Integer>();
    ArrayList<String> videoList = new ArrayList<String>();

    ArrayList<String> pdfList = new ArrayList<String>();
    ArrayList<String> type = new ArrayList<String>();


    Context context;


    public InnerModuleSubcategoryAdapter( Context context,ArrayList<String> arrayList, ArrayList<Integer> spinnerid,ArrayList<String> decrptionList,ArrayList<String> imageurl,ArrayList<String> videoList,ArrayList<String> pdfList,ArrayList<String> type) {




        this.arrayList = arrayList;
        this.descriptionList = decrptionList;
        this.context = context;
        this.spinnerid = spinnerid;
        this.imageurl = imageurl;
        this.videoList = videoList;
        this.pdfList = pdfList;
        this.type = type;

    }



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_list_sub_category_inner, parent, false);
        view.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT));
      ViewHolder myViewHolder = new ViewHolder(view);
        return myViewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {


      if(type.get(position).equals("0")){
        if(!imageurl.get(position).equals(context.getString(R.string.pureServer_link)+"null")) {


            Glide.with(context)
                    .load(imageurl.get(position))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.contentimageView);

        }else {
            if (!videoList.get(position).equals(context.getString(R.string.pureServer_link) + "null")) {


                holder.contentimageView.setImageResource(R.drawable.video);


            } else if (!pdfList.get(position).equals(context.getString(R.string.pureServer_link) + "null")) {


                holder.contentimageView.setImageResource(R.drawable.pdf);


            } else {
                holder.contentimageView.setImageResource(R.drawable.smlogo);

            }
        }}else{
          holder.contentimageView.setImageResource(R.drawable.specialpermission);

      }

        //  Glide.with(context).load(imageurl.get(position)).into(holder.contentimageView);
       holder.title.setText(arrayList.get(position));


        holder.detail.setText(descriptionList.get(position));




    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView title,detail;
        ImageView contentimageView;
        CardView cardView;

        public ViewHolder(@NonNull final View itemView) {
            super(itemView);


            title = itemView.findViewById(R.id.contentListTitleTextView);
            title.setSelected(true);

            detail = itemView.findViewById(R.id.contentListDescriptionTextView);

            contentimageView = itemView.findViewById(R.id.contentListImageView);

            cardView = itemView.findViewById(R.id.card_view_InnerModule);


            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int itemPosition = getAdapterPosition();


                    Bundle bundle = new Bundle();
                    bundle.putString("InnerModuleSubCategory",""+spinnerid.get(itemPosition));

                    Navigation.findNavController(view).getGraph().findNode(R.id.SingleContentFragment).setLabel(arrayList.get(itemPosition));


                    Navigation.findNavController(view).navigate(R.id.SingleContentFragment,bundle);




                }
            });



        }
        @Override
        public void onClick(View view) {

        }



    }
}
