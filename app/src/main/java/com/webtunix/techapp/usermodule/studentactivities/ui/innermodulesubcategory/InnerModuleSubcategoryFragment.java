package com.webtunix.techapp.usermodule.studentactivities.ui.innermodulesubcategory;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.Image;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.github.barteksc.pdfviewer.PDFView;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.google.android.youtube.player.YouTubePlayerView;
import com.webtunix.techapp.R;
import com.webtunix.techapp.usermodule.studentactivities.ui.modulesubcategory.ModuleSubCategoryAdapter;
import com.webtunix.techapp.usermodule.studentactivities.ui.modulesubcategory.ModuleSubcategoryFragment;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A simple {@link Fragment} subclass.
 */
public class InnerModuleSubcategoryFragment extends Fragment    {

    ArrayList<Bitmap> bitmaps;
    RecyclerView recyclerView;

    ArrayList<String> arrayList ;
    ArrayList<String> type ;

    ArrayList<Integer> spinnerId ;

    ArrayList<String> imageurl ;
    ArrayList<String> videolist ;
    ArrayList<String> descriptionList ;

    ArrayList<String> pdflist ;


    StringBuilder total;

AutoCompleteTextView  autoCompleteTextView;
    Context context;

    PDFView pdfView;
    ImageView imageView;

    TextView  textView, titletextview;

    WebView videoView;

    TextView emptyinnermodulesubcategory;
    View view;
    YouTubePlayerFragment youtubeFragment;

    InnerModuleSubcategoryAdapter  innermoduleSubCategoryAdapter;
    // YouTUbe Player Fargment
    private InnerModuleSubcategoryFragment myContext;

    private YouTubePlayer YPlayer;
    private static final String YoutubeDeveloperKey = "AIzaSyAqVTpWcJ9a6-HjsTcNyT_oAmLEYFArJbU";
    private static final int RECOVERY_DIALOG_REQUEST = 1;
String str,s1,pass;
    View root;
    String reg = "(?:youtube(?:-nocookie)?\\.com\\/(?:[^\\/\\n\\s]+\\/\\S+\\/|(?:v|e(?:mbed)?)\\/|\\S*?[?&]v=)|youtu\\.be\\/)([a-zA-Z0-9_-]{11})";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);

        // Inflate the layout for this fragment
       root = inflater.inflate(R.layout.fragment_innermodulesubcategory, container, false);


        SharedPreferences sh = getActivity().getSharedPreferences("techapppref",Context.MODE_PRIVATE);

        s1 = sh.getString("useremail", "");
        pass = sh.getString("userpassword", "");
        str = s1.trim()+":"+pass.trim();

        recyclerView  = root.findViewById(R.id.contentList_subcategorymoduleID);
        autoCompleteTextView  = root.findViewById(R.id.innersubcategorysearchAutoTextView);

        arrayList = new ArrayList<String>();
        descriptionList = new ArrayList<String>();
        videolist = new ArrayList<String>();
        pdflist = new ArrayList<String>();
        type = new ArrayList<String>();

        imageurl = new ArrayList<String>();
        bitmaps = new ArrayList<Bitmap>();

        spinnerId = new ArrayList<Integer>();
        emptyinnermodulesubcategory = root.findViewById(R.id.emptyInnersubcategory);
        DoSomething doSomething = new DoSomething();

        doSomething.execute(str);
        Log.d("Chaleya Ki Module wala",arrayList.toString());


        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                String data = (String) arg0.getAdapter().getItem(arg2);
                Log.d("String Data",""+data);


                int id =   arrayList.indexOf(data);

                Log.d("String arg",""+   spinnerId.get(arg2));


                Bundle bundle = new Bundle();
                bundle.putString("InnerModuleSubCategory",""+spinnerId.get(id));

                Navigation.findNavController(root).getGraph().findNode(R.id.SingleContentFragment).setLabel(arrayList.get(id));


                Navigation.findNavController(root).navigate(R.id.SingleContentFragment,bundle);



            }
        });

        autoCompleteTextView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {


                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(autoCompleteTextView.getApplicationWindowToken(), 0);


            }
        });


        return root;



    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    class DoSomething extends AsyncTask<String,Void,Void> {


        @Override
        protected Void doInBackground(String... strings) {

            try {
                byte[] data;
                String base64=null;



                try {

                    data = strings[0].getBytes("UTF-8");

                    base64 = Base64.encodeToString(data, Base64.DEFAULT);

                    Log.i("Base  ", base64);

                } catch (Exception e) {

                    e.printStackTrace();

                }

            Log.d("Getting Values : ",""+getArguments().getString("ModuleSubCategoryPosition").toString());
                URL obj = new URL(getString(R.string.server_link)+"Course_Content_Serializers_list/"+getArguments().getString("ModuleSubCategoryPosition"));

                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                con.setReadTimeout(20000);
                con.setConnectTimeout(20000);
                con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                con.addRequestProperty("Authorization", "Basic "+base64);

                con.setRequestProperty("Expect", "100-continue");
                int  responseCode = con.getResponseCode();
                System.out.println("Response Code :: " + responseCode);

                Log.i("Hazoor Sir Java ", responseCode + "");

                InputStream inputStream = con.getInputStream();

                BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
                total = new StringBuilder();
                for (String line; (line = r.readLine()) != null; ) {
                    total.append(line).append('\n');
                }

                Log.i("Vineet Kumar ", total + "");




            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {


                JSONObject jsonObject = new JSONObject(total.toString());

                JSONArray jsonArray = jsonObject.getJSONArray("data");

                for (int i = (jsonArray.length()-1); i >= 0; i--) {
                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);


                    arrayList.add(jsonObject2.optString("title").toString());
                    imageurl.add(getString(R.string.pureServer_link)+jsonObject2.optString("image").toString());
                    videolist.add(getString(R.string.pureServer_link)+jsonObject2.optString("video2").toString());

                    pdflist.add(getString(R.string.pureServer_link)+jsonObject2.optString("pdf").toString());
                    spinnerId.add((jsonObject2.optInt("id")));
                    descriptionList.add((jsonObject2.optString("details")));
                    type.add(jsonObject2.optString("type").toString());


                    Log.d("HELLO ARRAYLIST AAYI ",arrayList.toString());
                    Log.d("HELLO ARRAYLIST AAYI ",imageurl.toString());


                }

            }catch (Exception e){

            }

            if(arrayList.isEmpty()&&spinnerId.isEmpty()){

                emptyinnermodulesubcategory.setVisibility(View.VISIBLE);
            }else{
                emptyinnermodulesubcategory.setVisibility(View.GONE);

            }
            innermoduleSubCategoryAdapter = new InnerModuleSubcategoryAdapter(getContext(),arrayList,spinnerId,descriptionList,imageurl,videolist,pdflist,type);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setAdapter(innermoduleSubCategoryAdapter);

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,arrayList);
            autoCompleteTextView.setAdapter(adapter);





        }
    }
}
