package com.webtunix.techapp.usermodule.studentactivities.ui.gallery;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.webtunix.techapp.R;
import com.webtunix.techapp.usermodule.studentactivities.ui.modulesubcategory.ModuleSubCategoryAdapter;

import java.util.ArrayList;

public class NotificationAdapter  extends  RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    ArrayList<String> titlee = new ArrayList<String>();
    ArrayList<Integer> spinnerid = new ArrayList<Integer>();
    ArrayList<String> description = new ArrayList<String>();
    ArrayList<String> imageurl = new ArrayList<String>();




    Context context;


    public NotificationAdapter( Context context,ArrayList<String> title, ArrayList<Integer> spinnerid,ArrayList<String> description,ArrayList<String> imageurl) {




        this.titlee = title;
        this.context = context;
        this.spinnerid = spinnerid;
        this.description = description;
        this.imageurl = imageurl;


    }



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notificationlistrecycler, parent, false);
        view.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT));
        ViewHolder myViewHolder = new ViewHolder(view);
        return myViewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {



           if((imageurl.get(position).contains(".mp3"))||(imageurl.get(position).contains(".mp4"))){

               holder.contentimageView.setImageResource(R.drawable.video);

           }else if((imageurl.get(position).contains(".pdf"))) {

               holder.contentimageView.setImageResource(R.drawable.pdf);

           }else if((imageurl.get(position).contains("null"))) {

               holder.contentimageView.setImageResource(R.drawable.smlogo);

           }else{
               Glide.with(context)
                       .load(imageurl.get(position))
                       .diskCacheStrategy(DiskCacheStrategy.ALL)
                       .into(holder.contentimageView);
           }



        //  Glide.with(context).load(imageurl.get(position)).into(holder.contentimageView);
        holder.title.setText(titlee.get(position));


        holder.detail.setText(description.get(position));




        Log.d("Module Subcategory ","I am here only");





    }

    @Override
    public int getItemCount() {
        return titlee.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView title,detail;
        ImageView contentimageView;

        CardView cardView;

        public ViewHolder(@NonNull final View itemView) {
            super(itemView);


            title = itemView.findViewById(R.id.notificationListTitleTextView);
            title.setSelected(true);
            detail = itemView.findViewById(R.id.notificationListDescriptionTextView);

            contentimageView = itemView.findViewById(R.id.notificationListImageView);

            cardView = itemView.findViewById(R.id.card_view_Notification);

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int itemPosition = getAdapterPosition();



                    Bundle bundleString = new Bundle();
                    bundleString.putString("NotificationSingleID",""+spinnerid.get(itemPosition));

                    Navigation.findNavController(view).getGraph().findNode(R.id.notificationSinglePage).setLabel(titlee.get(itemPosition));

                    Navigation.findNavController(view).navigate(R.id.notificationSinglePage,bundleString);




                }
            });




        }
        @Override
        public void onClick(View view) {

        }



    }
}