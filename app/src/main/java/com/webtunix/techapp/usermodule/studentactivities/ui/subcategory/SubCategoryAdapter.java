package com.webtunix.techapp.usermodule.studentactivities.ui.subcategory;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.webtunix.techapp.R;


import java.util.ArrayList;

public class SubCategoryAdapter  extends  RecyclerView.Adapter<SubCategoryAdapter.ViewHolder> {

    ArrayList<String> arrayList = new ArrayList<String>();
    ArrayList<Integer> spinnerid = new ArrayList<Integer>();


    Context context;


    public SubCategoryAdapter( Context context,ArrayList<String> arrayList,  ArrayList<Integer> spinnerid) {




        this.arrayList = arrayList;

        this.context = context;
        this.spinnerid = spinnerid;

    }




    @Override
    public SubCategoryAdapter.ViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.subcategory_modules_layout, parent, false);
        view.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT));
        ViewHolder myViewHolder = new ViewHolder(view);
        return myViewHolder;

    }

    @Override
    public void onBindViewHolder( ViewHolder holder, int position) {


        holder.textView.setText(arrayList.get(position));
        Log.d("Main Sub Category","You find me in Binder ");


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView textView;
        public ViewHolder(final View itemView) {
            super(itemView);

            textView = itemView.findViewById(R.id.modulestextview);

            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                 Integer itemPosition = getAdapterPosition();


                 Bundle bundle = new Bundle();
                 bundle.putString("POSITION",""+spinnerid.get(itemPosition));
                 //  SubCategoryFragmentDirections.actionFragmentAToFragmentB(""+itemPosition);


                    Navigation.findNavController(view).getGraph().findNode(R.id.modulesubcategorynavid).setLabel(arrayList.get(itemPosition));

                    Navigation.findNavController(view).navigate(R.id.modulesubcategorynavid,bundle);

                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });

        }

        @Override
        public void onClick(View view) {


        }
    }

}
