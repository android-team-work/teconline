package com.webtunix.techapp.usermodule.studentactivities.ui.tools;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Base64OutputStream;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.webtunix.techapp.R;
import com.webtunix.techapp.usermodule.studentactivities.ui.home.HomeAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

import static android.content.Context.MODE_PRIVATE;


public class ToolsFragment extends Fragment {

    private ToolsViewModel toolsViewModel;
    StringBuilder total;
    int responseCode;

    EditText subject, message;
    String sender;
    Button button;
    ScrollView scrollView;
LinearLayout bottomNavigationView;

    String mailTo ="maneetkhurana@gmail.com";
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        toolsViewModel =
                ViewModelProviders.of(this).get(ToolsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_tools, container, false);
        bottomNavigationView = (LinearLayout) getActivity().findViewById(R.id.bottomViewRealtiveLayout);

        subject = root.findViewById(R.id.supportSubjectEditText);
        message = root.findViewById(R.id.supportMessageEditText);
        button = root.findViewById(R.id.SupportSendbtn);
        scrollView = root.findViewById(R.id.ToolFragmentScrollview);


       subject.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               bottomNavigationView.setVisibility(View.GONE);


           }
       });
        message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    bottomNavigationView.setVisibility(View.GONE);


            }
        });

       subject.setOnFocusChangeListener(new View.OnFocusChangeListener() {
           @Override
           public void onFocusChange(View v, boolean hasFocus) {
               bottomNavigationView.setVisibility(View.GONE);

               int viewHeight = scrollView.getMeasuredHeight();
               int contentHeight = scrollView.getChildAt(0).getHeight();
               if(viewHeight - contentHeight < 0) {

                   bottomNavigationView.setVisibility(View.GONE);

               }else{
                   bottomNavigationView.setVisibility(View.VISIBLE);
               }


           }
       });

        message.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                bottomNavigationView.setVisibility(View.GONE);


                int viewHeight = scrollView.getMeasuredHeight();
                int contentHeight = scrollView.getChildAt(0).getHeight();
                if(viewHeight - contentHeight < 0) {

                    bottomNavigationView.setVisibility(View.GONE);

                }


            }
        });

        subject.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                bottomNavigationView.setVisibility(View.GONE);

                if(!(subject.getText()==null)){

                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(subject.getApplicationWindowToken(), 0);

                }
            }
        });



     /*   message.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(!(message.getText()==null)){

                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(message.getApplicationWindowToken(), 0);

                }
            }
        });*/





       button.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Intent it = new Intent(Intent.ACTION_SEND);
               it.putExtra(Intent.EXTRA_EMAIL, new String[]{ mailTo});
               it.putExtra(Intent.EXTRA_SUBJECT,subject.getText().toString());
               it.putExtra(Intent.EXTRA_TEXT,message.getText());
               it.setType("message/rfc822");
               startActivity(Intent.createChooser(it,"Choose Mail App"));

               Navigation.findNavController(view).navigate(R.id.nav_home);


           }
       });


                return root;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        bottomNavigationView.setVisibility(View.VISIBLE);
    }
}