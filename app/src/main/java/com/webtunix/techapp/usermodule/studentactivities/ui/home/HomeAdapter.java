package com.webtunix.techapp.usermodule.studentactivities.ui.home;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.navigation.NavigationView;
import com.squareup.picasso.Picasso;
import com.webtunix.techapp.R;
import com.webtunix.techapp.usermodule.studentactivities.WelcomeStudent;
import com.webtunix.techapp.usermodule.studentactivities.ui.send.SendFragment;
import com.webtunix.techapp.usermodule.studentactivities.ui.subcategory.SubCategoryFragment;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.BitSet;

import static com.webtunix.techapp.usermodule.studentactivities.ui.home.HomeFragment.getNameValue;


public class HomeAdapter extends  RecyclerView.Adapter<HomeAdapter.ViewHolder> {

    ArrayList<String> arrayList = new ArrayList<String>();
    ArrayList<String> imageurl = new ArrayList<String>();
    ArrayList<Integer> spinnerid = new ArrayList<Integer>();

    Bitmap bitmap = null;
    InputStream stream = null;
    BitmapFactory.Options bmOptions = new BitmapFactory.Options();

    HomeViewModel homeViewModel;

    Activity context;


    public HomeAdapter( Activity context,ArrayList<String> arrayList, ArrayList<String> imageurl, ArrayList<Integer> spinnerid) {




        this.arrayList = arrayList;
        this.imageurl = imageurl;
         this.context = context;
         this.spinnerid = spinnerid;

    }



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.maincategory_layout, parent, false);
        view.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT));
        ViewHolder myViewHolder = new ViewHolder(view);
        return myViewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {



        Glide.with(context)
                .load(imageurl.get(position))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.imageView);







             holder.textView.setText(arrayList.get(position));




    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imageView;
        TextView textView;

        public ViewHolder(@NonNull final View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.mainscreencategoryImage);

            textView = itemView.findViewById(R.id.mainCategoryTitle);
            textView.setSelected(true);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int itemPosition = getAdapterPosition();

                    Bundle bundle = new Bundle();



                    Bundle bundle1 = new Bundle();
                    bundle.putString("SubCAtegoryID",""+spinnerid.get(itemPosition));
                    bundle.putString("LabelHomeCategory",arrayList.get(itemPosition));

                    HomeFragment.getNameValue(itemPosition);
                    Navigation.findNavController(view).getGraph().findNode(R.id.subCategory).setLabel(arrayList.get(itemPosition));
                  Navigation.findNavController(view).navigate(R.id.subCategory,bundle);




                }

            });
        }
        @Override
        public void onClick(View view) {



        }



    }


}
