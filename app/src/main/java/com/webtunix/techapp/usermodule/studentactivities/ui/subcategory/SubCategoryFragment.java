package com.webtunix.techapp.usermodule.studentactivities.ui.subcategory;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.webtunix.techapp.R;
import com.webtunix.techapp.usermodule.studentactivities.ui.home.HomeAdapter;
import com.webtunix.techapp.usermodule.studentactivities.ui.send.SendFragment;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;


public class SubCategoryFragment extends Fragment {


    // TODO: Rename and change types of parameters


    SubCategoryAdapter subCategoryAdapter;

    ArrayList<Bitmap> bitmaps;
    RecyclerView recyclerView;

    ArrayList<String> arrayList ;
    ArrayList<Integer> spinnerId ;

    ArrayList<String> imageurl ;

    StringBuilder total;

    TextView emptysubcategory;

TextView textView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        // Inflate the layout for this fragment
        View root =  inflater.inflate(R.layout.fragment_sub_category, container, false);

        recyclerView  = root.findViewById(R.id.subcategorymoduleID);
        arrayList = new ArrayList<String>();
        imageurl = new ArrayList<String>();
        bitmaps = new ArrayList<Bitmap>();

emptysubcategory = root.findViewById(R.id.emptysubcategory);
        spinnerId = new ArrayList<Integer>();


        DoSomething doSomething = new DoSomething();

        doSomething.execute();

        recyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                        int itemPosition = recyclerView.getChildAdapterPosition(view);

                        Log.d("Have I clicked",""+itemPosition);

                        Navigation.findNavController(view).navigate(R.id.modulesubcategorynavid);


            }
        });



        return root;

    }
    class DoSomething extends AsyncTask<Void,Void,Void> {


        @Override
        protected Void doInBackground(Void... voids) {

            try {

//                String urlString = getArguments().getString("url");

                URL obj = new URL(getString(R.string.server_link)+"Super_Course_Category_Serializers_list/"+getArguments().getString("SubCAtegoryID").toString());

                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                con.setReadTimeout(20000);
                con.setConnectTimeout(20000);
                con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                con.setRequestProperty("Expect", "100-continue");
                int  responseCode = con.getResponseCode();
                System.out.println("Response Code :: " + responseCode);

                Log.i("Hazoor Sir Java ", responseCode + "");

                InputStream inputStream = con.getInputStream();

                BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
                total = new StringBuilder();
                for (String line; (line = r.readLine()) != null; ) {
                    total.append(line).append('\n');
                }

                Log.i("Vineet Kumar ", total + "");




            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {


                JSONObject jsonObject = new JSONObject(total.toString());

                JSONArray jsonArray = jsonObject.getJSONArray("data");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);


                    arrayList.add(jsonObject2.optString("title").toString());

                    spinnerId.add((jsonObject2.optInt("id")));


                    Log.d("HELLO ARRAYLIST AAYI ",arrayList.toString());
                    Log.d("HELLO ARRAYLIST AAYI ",spinnerId.toString());


                }

            }catch (Exception e){

            }

            if(arrayList.isEmpty()&&spinnerId.isEmpty()){

                emptysubcategory.setVisibility(View.VISIBLE);
            }else{
                emptysubcategory.setVisibility(View.GONE);

            }

            subCategoryAdapter = new SubCategoryAdapter(getActivity(),arrayList,spinnerId);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setAdapter(subCategoryAdapter);

            recyclerView.addItemDecoration(new MarginItemDecoration(1));




        }
    }

}
