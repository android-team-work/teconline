package com.webtunix.techapp;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.webtunix.techapp.prelogin.SignIn;
import com.webtunix.techapp.usermodule.studentactivities.WelcomeStudent;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    String username;
                       PendingIntent pendingIntent;
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

                                  SharedPreferences sh = getSharedPreferences("techapppref",MODE_PRIVATE);
                                        username = sh.getString("useremail", "");

                                        if(username.equals("null")){
                                                 Intent intent = new Intent(MyFirebaseMessagingService.this, SignIn.class);
                                                           intent.putExtra("NewNotification","NotificationSingle")   ;

                                                                     intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                                                                         | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                                          pendingIntent = PendingIntent.getActivity(MyFirebaseMessagingService.this, 0, intent, 0);

                                        }else {

                                            Intent intent = new Intent(MyFirebaseMessagingService.this, WelcomeStudent.class);

                                            intent.putExtra("NewNotification", "NotificationSingle");
                                     
                                     

                                         pendingIntent = PendingIntent.getActivity(MyFirebaseMessagingService.this, 0, intent, 0);


                                        }

             if (remoteMessage.getNotification() != null) {
            Log.d("TESTING NOTIFICATION", "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }



             
      NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(MyFirebaseMessagingService.this, "channel_id")
                .setContentTitle(remoteMessage.getNotification().getTitle())
                .setContentText(remoteMessage.getNotification().getBody())
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setStyle(new NotificationCompat.BigTextStyle())
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setSmallIcon(R.mipmap.logo)
              .setBadgeIconType(NotificationCompat.BADGE_ICON_LARGE)
              .setColor(ContextCompat.getColor(MyFirebaseMessagingService.this, R.color.colorAccent))
              .setContentIntent(pendingIntent)

                .setAutoCancel(true);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());

          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {



              NotificationChannel channel = new NotificationChannel("channel_id",
                            "Channel human readable title",
                            NotificationManager.IMPORTANCE_HIGH);
              channel.setShowBadge(true);

              notificationManager.createNotificationChannel(channel);

          }


    }
}

